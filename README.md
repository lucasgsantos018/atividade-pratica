# Atividade Pratica

Repositório destinado a atividades práticas do processo seletivo TerraLab

![Gif](https://i.pinimg.com/originals/78/e6/e0/78e6e06190fdc66cbdc2925ed37ff781.gif)

## Questões sprint 1

3) Esse código pega dois números inteiros, passados por argumentos no terminal e efetua a soma entre eles.
Para executar basta escrever node calculadora.js [PARÂMETRO A] [PARÂMETRO B], em que:
A -> É um número inteiro
B -> É um número inteiro

4) Para adicionar somente um arquivo ao commit, basta executar o comando git add nomeDoArquivo e em seguida fazer o commit
O commit da calculadora.js deverá ser do tipo FEAT, pois adiciona um novo recurso na base de código
O commit do README.md deverá ser do tipo DOCS, pois indicam que houveram mudanças na documentação, ou seja, não incluem alterações em código 

5) Essa mudança deixa de ser um FEAT e passa a ser um REFACTOR, pois deixa de ser uma nova função para ser uma reestruturação de código

6) O erro é no argumento do switch, ao invés de passar o argumento[2], está sendo passado o argumento 0

A execução fica: node calculadora.js [PARÂMETRO A] [PARÂMETRO B] [PARÂMETRO C] em que,
A -> É um número inteiro.
B -> É um número inteiro.
C -> É a operação a ser efetuada (soma e sub). 

10) O comando para execução é

node calculadora.js [PARÂMETRO A] [PARÂMETRO B] [PARÂMETRO C]

A -> É um número inteiro
B -> É um operador (soma +, subtração -, divisão /)
C -> É um número inteiro

João não fez a leitura de dados no código, portanto não há definição para args, ou seja, para o código se tornar funcional há a necessidade de fazer a leitura dos argumentos utilizando -> const args = process.argv.slice(2);
A função eval() lê uma string e separa seus argumentos